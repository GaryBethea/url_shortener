# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
UrlShortener::Application.config.secret_key_base = '393762f1ec185c5c4390cc51e5fe0662bf15216a9d5b218862bf39453b37fa00520c514f17b0a9c9be667eaf5a24524f2fd4615c1cfb43f308697bf022d08f32'
