class ShortenedUrl < ActiveRecord::Base
  validates :submitter_id, presence: true
  validates :short_url, presence: true, uniqueness: true

  def self.random_code
    code = SecureRandom.urlsafe_base64
    raise 'code already taken' if self.exists?(short_url: code)
    return code
  rescue StandardError
    retry
  end

  def self.create_for_user_and_long_url!(user, long_url)
    self.create!(
      long_url: long_url,
      short_url: self.random_code,
      submitter_id: user.id
    )
  end

  belongs_to(
    :submitter,
    class_name: "User",
    foreign_key: :submitter_id,
    primary_key: :id
  )

  has_many(
    :visits,
    class_name: "Visit",
    foreign_key: :shortened_url_id,
    primary_key: :id
  )

  has_many :visitors, through: :visits, source: :user

  def num_clicks
    visits.count
  end

  def num_uniques
    visits.select(:user_id).distinct.count
  end

  def num_recent_uniques
    visits.select(:user_id).distinct.where(created_at: (Time.now - 600)..Time.now).count
  end

end